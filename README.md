## Event grammar data model

This is a very slightly tweaked implementation of
[this event grammar](https://snowplowanalytics.com/blog/2013/08/12/towards-universal-event-analytics-building-an-event-grammar/).

The primary differences are as follows:

- There are two different types of nouns but they can be used as the subject
  or thing in the event grammar.
    - Entity. An entity is an animate object that is most often the subject rather
      than the object. For example people and animals are Entities
    - Things. Things are most usually inanimate and are often the object in
      the event grammar.
- Most of the edges were renamed to be more explicit.

### Neo4j schema for event grammar

The following example is both an expression of the grammar as well as an actual
instance of it. This is actually self-documenting... kinda nuts, right?

You can install and run neo4j and copy the following commands into the browser
command prompt to create a dataset for query POC purposes. The dataset is supposed
to be as simple as possible while having enough different scenarios to cover
more complex query testing.


```

// MATCH (a) DETACH DELETE a
// MATCH (a) RETURN a

// create the Things
CREATE (t0:Thing{id:0, type: 'page'})
CREATE (t1:Thing{id:1, type: 'page'})
CREATE (t2:Thing{id:2, type: 'page'})
CREATE (t3:Thing{id:3, type: 'page'})
CREATE (t4:Thing{id:4, type: 'page'})
CREATE (t5:Thing{id:5, type: 'page'})
CREATE (t6:Thing{id:6, type: 'page'})
CREATE (t7:Thing{id:7, type: 'page'})
CREATE (t8:Thing{id:8, type: 'page'})

CREATE (e0:Entity{id:0, type: 'user'})
CREATE (e1:Entity{id:1, type: 'user'})
CREATE (e2:Entity{id:2, type: 'user'})
CREATE (e3:Entity{id:3, type: 'user'})

CREATE (a0:Action{id:0, object_id: 0, verb: 'view'})
CREATE (a1:Action{id:1, object_id: 1, verb: 'view'})
CREATE (a2:Action{id:2, object_id: 5, verb: 'view'})
CREATE (a3:Action{id:3, object_id: 2, verb: 'view'})
CREATE (a4:Action{id:4, object_id: 3, verb: 'view'})
CREATE (a5:Action{id:5, object_id: 4, verb: 'view'})
CREATE (a6:Action{id:6, object_id: 5, verb: 'view'})
CREATE (a7:Action{id:7, object_id: 5, verb: 'view'})
CREATE (a8:Action{id:8, object_id: 6, verb: 'view'})
CREATE (a9:Action{id:9, object_id: 7, verb: 'view'})
CREATE (a10:Action{id:10, object_id: 5, verb: 'view'})
CREATE (a11:Action{id:11, object_id: 6, verb: 'view'})
CREATE (a12:Action{id:12, object_id: 7, verb: 'view'})
CREATE (a13:Action{id:13, object_id: 8, verb: 'view'})

// create the actions and associated them with Things

// entity 0
CREATE (e0)-[:SUBJECT_OF]->(a0)-[:HAS_OBJECT]->(t0)
CREATE (e0)-[:SUBJECT_OF]->(a1)-[:HAS_OBJECT]->(t1)
CREATE (e0)-[:SUBJECT_OF]->(a2)-[:HAS_OBJECT]->(t5)

CREATE (a1)-[:PREV]->(a0)
CREATE (a2)-[:PREV]->(a1)


// entity 1
CREATE (e1)-[:SUBJECT_OF]->(a3)-[:HAS_OBJECT]->(t2)
CREATE (e1)-[:SUBJECT_OF]->(a4)-[:HAS_OBJECT]->(t3)
CREATE (e1)-[:SUBJECT_OF]->(a5)-[:HAS_OBJECT]->(t4)
CREATE (e1)-[:SUBJECT_OF]->(a6)-[:HAS_OBJECT]->(t5)

CREATE (a4)-[:PREV]->(a3)
CREATE (a5)-[:PREV]->(a4)
CREATE (a6)-[:PREV]->(a5)

// entity 2
CREATE (e2)-[:SUBJECT_OF]->(a7)-[:HAS_OBJECT]->(t5)
CREATE (e2)-[:SUBJECT_OF]->(a8)-[:HAS_OBJECT]->(t6)
CREATE (e2)-[:SUBJECT_OF]->(a9)-[:HAS_OBJECT]->(t7)
CREATE (a7)-[:PREV]->(a8)
CREATE (a8)-[:PREV]->(a9)


// entity 3
CREATE (e3)-[:SUBJECT_OF]->(a10)-[:HAS_OBJECT]->(t5)
CREATE (e3)-[:SUBJECT_OF]->(a11)-[:HAS_OBJECT]->(t6)
CREATE (e3)-[:SUBJECT_OF]->(a12)-[:HAS_OBJECT]->(t7)
CREATE (e3)-[:SUBJECT_OF]->(a13)-[:HAS_OBJECT]->(t8)

CREATE (a10)-[:PREV]->(a11)
CREATE (a11)-[:PREV]->(a12)
CREATE (a12)-[:PREV]->(a13)

```

After the above queries have run, you can select the entire contents of the
dataset and see the output. Be sure to see the color coding at the top of the
image that denotes Entity, Thing, and Action 

![toy dataset](http://i.imgur.com/JoQ5iov.png)


## Queries

```

// Get all pageviews connected to an item
MATCH path=(t:Thing{id: 5})<-[:HAS_OBJECT]-(Action)
RETURN path

// get most common previously visited object
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV]->(a:Action)
RETURN a.object_id, COUNT(a.object_id)
ORDER BY COUNT(a.object_id) DESC

// get most common previously visited object within 3 actions
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV*1..3]->(a:Action)
RETURN a.object_id, COUNT(a.object_id)
ORDER BY COUNT(a.object_id) DESC

// go one action back in history
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV]->(a:Action)
RETURN path

// return all paths of any length
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV*]->(a:Action)
RETURN path

// count # of actions per object that were visited before
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV*]->(a:Action)
RETURN a.object_id, COUNT(a.object_id)
ORDER BY COUNT(a.object_id) DESC

// return counts of all paths and subpaths taken
// PROBABLY VERY EXPENSIVE!!!
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV*]->(a:Action)
WITH EXTRACT(n IN NODES(path)[2..] | n.object_id) AS object_path
RETURN COUNT(object_path), object_path
ORDER BY COUNT(object_path) DESC

// common length of paths leading up to the object
MATCH path=(:Thing{id: 5})<-[:HAS_OBJECT]-(:Action)-[:PREV*]->(a:Action)
WITH SIZE(NODES(path)[2..]) AS pathlen
RETURN pathlen, COUNT(pathlen)
LIMIT 10

// To get outgoing, reverse the direction of the :PREV edge
```
